1.Buat Database
    CREATE DATABASE myshop;

2. Membuat Tabel
    a. Users
        CREATE TABLE users (
            id INT NULL AUTO_INCREMENT KEY,
            name varchar(255),
            email varchar(255),
            password varchar(255)
        );
    b. Items
        CREATE TABLE items (
            id INT NULL AUTO_INCREMENT KEY,
            name varchar(255),
            description varchar(255),
            price int,
            stock int,
            category_id int,
            FOREIGN KEY (category_id) REFERENCES categories(id)
        );
    c. Categoris
        CREATE TABLE categories (
            id INT NULL AUTO_INCREMENT KEY,
            name varchar(255)
        );

3.Insert Database
    a. users
        INSERT INTO users VALUES 
        (1,"John Doe", "john@doe.com", "john123"),
        (2,"Jane Doe", "jane@doe.com", "jenita123");
    b.categories
        INSERT INTO categories
        VALUES (1,"gadget"),
        (2,"cloth"),(3,"men"),
        (4,"women"),(5,"branded");
    c.items
        INSERT INTO items
        VALUES (1,"sumsang b50","hape keren dari merek sumsang",4000000,100,1),
        (2,"uniklooh","baju keren dari brand ternama",500000,50,2),
        (3,"IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil Data
    a.Mengambil data users
        SELECT id,name,email FROM `users`; 
    b. Mengambil data items
        1. Harga diatas 1 jt 
            SELECT * FROM `items` WHERE price >1000000;
        2. dengan kata kunci Watch
            SELECT * FROM `items` WHERE `name` LIKE '%Watch%';
    c. Menampilkan Data dengan Inner join
        SELECT items.name,description,price,stock,category_id,
        categories.name AS Kategori FROM `items` INNER JOIN `categories`
        ON items.category_id=categories.id;
        
5. Ubah Data
    UPDATE items SET  id= 1,name="Sumsang b50",
    description="hape kren dari merek sumsang",price = 2500000, 
    stock=100,category_id=1 WHERE id=1;